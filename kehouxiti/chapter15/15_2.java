import java.util.*;
class student implements Comparable {
    int english=0;
    String name;
    student(int english,String name) {
        this.name=name;
        this.english=english;
    }
    public int compareTo(Object b) {
        student st=(student)b;
        return (this.english-st.english);
    }
}
public class 15_2 {
    public static void main(String args[]) {
        List<student> list=new LinkedList<student>();
        int score []={65,76,45,99,77,88,100,79};
        String name[]={"张三","李四","旺季","加戈","为哈","周和","赵李","将集"};
        for(int i=0;i<score.length;i++){
            list.add(new student(score[i],name[i]));
        }
        Iterator<student> iter=list.iterator();
        TreeSet<student> mytree=new TreeSet<student>();
        while(iter.hasNext()){
            student stu=iter.next();
            mytree.add(stu);
        }
        Iterator<student> te=mytree.iterator();
        while(te.hasNext()) {
            student stu=te.next();
            System.out.println(""+stu.name+" "+stu.english);
        }
    }
}
