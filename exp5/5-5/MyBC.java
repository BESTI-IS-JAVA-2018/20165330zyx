public class MyBC {
    public static String toPostfix(String expr){
        MyStack<String> stack = new MyStack<>(expr.length());
        String postfix = "";
        int i = 0;
        while(i<expr.length()){
            char ch = expr.charAt(i);
            switch (ch){
                case  '+':
                case  '-':while(!stack.isEmpty() && !stack.get().equals("(")) {
                    postfix += stack.pop();
                }
                    //postfix += " ";
                    stack.push(ch + "");
                    i++;
                    break;
                case '*':
                case '/':while (!stack.isEmpty() && (stack.get().equals("*")||stack.get().equals("/"))) {
                    postfix += stack.pop();
                }
                    //postfix += " ";
                    stack.push(ch + "");
                    i++;
                    break;
                case '(':stack.push(ch + "");
                    i++;
                    break;
                case ')':String out = stack.pop();
                    while(out!=null && !out.equals("(")){
                        postfix += out;
                        out = stack.pop();
                        //postfix += " ";
                    }
                    i++;
                    break;
                default:while(i < expr.length() && ch>='0' && ch<='9'){
                    postfix += ch;
                    i++;
                    if(i<expr.length())
                        ch = expr.charAt(i);
                }
                postfix += " ";
            }
        }
        while (!stack.isEmpty())
            postfix += stack.pop();
        return postfix;
    }
}

