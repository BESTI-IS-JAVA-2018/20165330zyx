import java.io.IOException;
import java.util.Scanner;
import java.lang.String;

public class MyBCTest {
    public static void main(String[] args)
            throws IOException {
        String expression, again;
        String infix;
        int result;
        try {
            Scanner in = new Scanner(System.in);
            do {
                MyDC evaluator = new MyDC();
                System.out.println("Enter a valid infix expression: ");
                expression = in.nextLine();
                MyBC theTrans = new MyBC(expression);
                infix = theTrans.doTrans();
                StringBuilder newInfix = new StringBuilder(infix.replace(" ",""));
                for (int i = 1; i < infix.length()+(i+1)/2 ; i=i+2) {
                    newInfix.insert(i," ");
                }
                System.out.println("Postfix is " + newInfix);
                result = evaluator.evaluate(newInfix.toString());
                System.out.println("That expression equals " + result);
                System.out.print("Evaluate another expression [Y/N]? ");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("y"));
        } catch (Exception IOException) {
            System.out.println("Input exception reported");
        }
    }
}
