class OutClass {
   int m = 1;
   static float x;   //A
   class InnerClass {
     int m = 12;       //B
     static float n = 20.89f;  //C
     InnerClass() {
     }
     void f() {
       m = 100;
     }
   }
   void cry() {
     InnerClass tom = new InnerClass(); //D
   }
}
