abstract class MotorVehicles{
	abstract void brake();
}
interface MoneyFare{
	void charge();
}
interface ControlTemperatory{
	void ControlAirTemperatory();
}
class Bus extends MotorVehicles implements MoneyFare{
	void brake(){
		System.out.println("公交车使用毂式刹车技术");
	}
	public void charge(){
		System.out.println("公共汽车：一元/张，不计公里数");
	}
}
class Taxi extends MotorVehicles implements MoneyFare,ControlTemperatory{
	void brake(){
		System.out.println("出租车使用盘式刹车技术");
	}
	public void charge(){
		System.out.println("出租车:2元/公里,起价3公里");
	}
	public void ControlAirTemperatory(){
		System.out.println("出租车安装了hair空调");
	}
}
class Cinema implements MoneyFare,ControlTemperatory{
	public void charge(){
		System.out.println("电影院：门票，十元/张");
	}
	public void ControlAirTemperatory(){
		System.out.println("电影院安装了中央空调");
	}
}
public class Example6_3{
	public static void main(String[] args) {
		Bus bus101=new Bus();
		Taxi bluetaxi=new Taxi();
		Cinema redStarCinema=new Cinema();
		MoneyFare fare;
		ControlTemperatory temperatory;
		fare=bus101;
		bus101.brake();
		fare.charge();
		fare=bluetaxi;
		temperatory=bluetaxi;
		bluetaxi.brake();
		fare.charge();
		temperatory.ControlAirTemperatory();
		fare=redStarCinema;
		temperatory=redStarCinema;
		fare.charge();
		temperatory.ControlAirTemperatory();
	}
}
