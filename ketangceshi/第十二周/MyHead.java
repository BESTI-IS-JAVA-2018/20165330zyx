import java.io.*;

import java.util.*;

public class MyHead {

    public static void main(String[] args) {

        int n = 0;

        for(String arg:args) {

            n = Integer.parseInt(arg);

        }

        File source = new File("20165330.txt");

        File target = new File("Out.txt");

        try {

            Writer out = new FileWriter(target);

            BufferedWriter bufferedWrite = new BufferedWriter(out);

            Reader in = new FileReader(source);

            BufferedReader bufferedRead = new BufferedReader(in);

            String str = null;

            for(int j=0;j<n;j++){

                if((str = bufferedRead.readLine())!=null){

                    bufferedWrite.write(str);

                    bufferedWrite.newLine();

                }

            }

            bufferedWrite.close();

            out.close();

            String s = null;

            System.out.println("前" + n + "行:");

            for (int j = 0; j < n; j++) {

                if ((s=bufferedRead.readLine())!= null) {

                    System.out.println(s);

                }

            }

            bufferedRead.close();

            in.close();

        }

        catch (IOException e){

            System.out.println(e.toString());

        }

    }

}

