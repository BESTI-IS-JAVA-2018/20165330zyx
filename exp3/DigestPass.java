import java.security.*;
/**
 * Demo class
 *
 * @author zyx
 * @date 2018/04/28
 */
public class DigestPass{
    public static void main(String[ ] args) throws Exception{
        String x = getString(args[0]);
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(x.getBytes("UTF8"));
        byte[] s = getDigest(m);
        String result = getString(getString());
        for (int i=0; i<s.length; i++){
            result+=Integer.toHexString((0x000000ff & s[i]) |
                    0xffffff00).substring(6);
        }
        System.out.println(result);
    }

    private static byte[] getDigest(MessageDigest m) {
        return m.digest();
    }

    private static String getString(String arg) {
        return arg;
    }

    private static String getString() {
        return "";
    }
}
